// Import Angular Core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import Containers
import { DefaultLayoutComponent } from './containers';
// Import Components
import { P404Component } from './core/error/404.component';
import { P500Component } from './core/error/500.component';
import { UserLoginComponent } from './modules/m1_module/user/user_login/user.login.component';
import {AuthGuardService} from "./core/auth/auth_guard.service";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: UserLoginComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'm1',
        loadChildren: () => import('./modules/m1_module/m1.module').then(m => m.M1Module)
      },
      {
        path: 'm2',
        loadChildren: () => import('./modules/m2_module/m2.module').then(m => m.M2Module)
      },
      {
        path: 'm3',
        loadChildren: () => import('./modules/m3_module/m3.module').then(m => m.M3Module)
      },
      {
        path: 'm6',
        loadChildren: () => import('./modules/m6_module/m6.module').then(m => m.M6Module)
      }
    ],
    canActivateChild: [AuthGuardService]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
