export interface AlertType {
  type: string;
  title: string;
  text: string;
}
