// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
// Import Internal Modules
import {AlertService} from './alert.service';
import {AlertType} from "./alert.type";

@Component({
  selector: 'alert-box',
  templateUrl: 'alert.component.html',
  styleUrls: ['alert.component.scss'],
})
export class AlertComponent implements OnInit, OnDestroy {
  private alertSubscription?: Subscription;
  alerts = new Array<AlertType>();
  dismissible = true;
  alertTimeout = 5000;

  constructor(private alertService: AlertService) {
  }

  ngOnInit() {
    this.alertSubscription = this.alertService.getAlert()
      .subscribe(message => {
        if (message) {
          this.alerts.push({type: message.type, title: message.title, text: message.text});
        }
      });
  }

  ngOnDestroy() {
    this.alertSubscription?.unsubscribe();
  }

  onClosed(dismissedAlert: AlertType): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
