// Import Core Module
import {Injectable} from '@angular/core';
import {Router, NavigationStart} from '@angular/router';
import {Observable, Subject} from 'rxjs';
// Import Internal Module
import {AlertType} from "./alert.type";

@Injectable({providedIn: 'root'})
export class AlertService {
  private alertSubject = new Subject<AlertType>();
  private keepAfterRouteChange = false;

  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterRouteChange) {
          // only keep for a single route change
          this.keepAfterRouteChange = false;
        } else {
          // clear alert message
          this.clear();
        }
      }
    });
  }

  getAlert(): Observable<AlertType> {
    return this.alertSubject.asObservable();
  }

  success(text: string, keepAfterRouteChange = true) {
    this.message('success', 'Success!', text, keepAfterRouteChange);
  }

  info(text: string, keepAfterRouteChange = true) {
    this.message('info', 'Info!', text, keepAfterRouteChange);
  }

  warning(text: string, keepAfterRouteChange = true) {
    this.message('warning', 'Warning!', text, keepAfterRouteChange);
  }

  error(text: string, keepAfterRouteChange = true) {
    this.message('danger', 'Error!', text, keepAfterRouteChange);
  }

  private message(type: string, title: string, text: string, keepAfterRouteChange = false) {
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.alertSubject.next({type: type, title: title, text: text});
  }

  clear() {
    // clear by calling subject.next() without parameters
    this.alertSubject.next();
  }
}
