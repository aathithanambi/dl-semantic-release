/**
 * @file Type declaration for Table Type.
 */

export interface ColumnDefinition {
  id: string,
  name: string,
  type: string
}
