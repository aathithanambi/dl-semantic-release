/**
 * @file Type declaration for Common Response Type.
 */

// Response type of `all` endpoint request
export interface AllResponseType<T> {
  entityData: T[],
  currentCount: number,
  totalCount: number

  [key: string]: T[] | number
}
