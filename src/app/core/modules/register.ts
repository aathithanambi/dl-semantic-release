// Import Core Modules
import {Subject} from "rxjs";
import {OnDestroy, OnInit} from "@angular/core";

// Base Register Class
export abstract class AbstractRegister<T> implements OnInit, OnDestroy{
  isLoading: boolean = false; // Progress Bar
  loadingMessage: string = 'Loading Registration Page...'; // Progress Bar Message
  destroy$: Subject<boolean> = new Subject<boolean>();

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  // extract specific fields from form data
  getFormData(formData: T, keys: (keyof T)[]): T {
    return Object.assign({}, ...keys.map((key) => ({[key]: formData[key]})));
  }

  showProgressBar(flag: boolean, message?: string) {
    this.isLoading = flag;
    if (message) {
      this.loadingMessage = message;
    }
  }
}
