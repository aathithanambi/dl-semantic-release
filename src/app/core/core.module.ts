import {NgModule} from '@angular/core';
import {AlertModule} from 'ngx-bootstrap/alert';
import {P404Component} from "./error/404.component";
import {P500Component} from "./error/500.component";
import {AlertComponent} from "./alert/alert.component";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [
    AlertModule.forRoot(),
    CommonModule
  ],
  exports: [
    AlertComponent
  ],
  declarations: [
    AlertComponent,
    P404Component,
    P500Component
  ]
})
export class CoreModule {
}
