// Import Core Modules
import {Injectable} from "@angular/core";
import {Router, CanActivate, CanActivateChild} from "@angular/router";

// Import Internal Modules
import {UserService} from "../../modules/m1_module/user/user.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(private readonly router: Router, private readonly userService: UserService) {
  }

  canActivate(): boolean {
    return this.isAuthenticated();
  }

  canActivateChild() {
    return this.isAuthenticated();
  }

  private isAuthenticated() {
    const currentUser = this.userService.currentUserValue;
    // authorised so return true
    if (currentUser) {
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(["/login"]);
    return false;
  }
}
