// Import Core Modules
import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';
// Import Internal Modules
import {environment} from "../../../environments/environment";
import {UserService} from "../../modules/m1_module/user/user.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private userService: UserService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = this.userService.currentUserValue;
    let token = environment.token.app;
    if (currentUser && currentUser.user_token) {
      token = currentUser.user_token;
    }
    request = request.clone({
      setHeaders: {
        'Content-Type': 'application/json',
        'x-auth-token': `${token}`
      }
    });

    return next.handle(request);
  }
}
