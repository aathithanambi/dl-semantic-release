// Import Angular Core
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// Import Internal Modules
import {MetadataType} from "./metadata.type";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class MetadataService {
  metadataUrl = environment.apiUrl + 'm0/dashboard/metadata/search';

  constructor(private http: HttpClient) {
  }

  // Get metadata by collection name
  getMetadata(collection_name: string): Observable<MetadataType<any>> {
    return this.http.get<MetadataType<any>>(this.metadataUrl + '/?name=' + collection_name);
  }
}
