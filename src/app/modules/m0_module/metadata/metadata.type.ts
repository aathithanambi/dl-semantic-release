/**
 * @file Type declaration for Metadata.
 */

// Default Type for metadata
type DefaultType<T> = {
  [K in keyof T]?: any
} & {
  [key: string]: any
}

// MongoDB Metadata Type
export type MetadataType<T> = {
  collection_name: string,
  sequence: string,
  default: DefaultType<T>

  [key: string]: string | DefaultType<T>
}
