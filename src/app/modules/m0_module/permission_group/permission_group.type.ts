// Permission Group Type Definition
export interface PermissionGroupType {
  _id?: string;
  permission_group_no?: string;
  permission_group_name?: string;
  permission_group_description?: string;
  permission_group_status_is_active?: boolean;
  permission_group_created_date?: Date;
  permission_group_modified_date?: Date;
}
