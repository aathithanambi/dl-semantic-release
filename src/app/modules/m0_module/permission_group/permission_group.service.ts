// Import Angular Core
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// Import Internal Modules
import {PermissionGroupType} from "./permission_group.type";
import {environment} from "../../../../environments/environment";
import {AllResponseType} from "../../../core/types/response_type";

@Injectable({
  providedIn: 'root',
})
export class PermissionGroupService {
  permissionGroupUrl = environment.apiUrl + 'm0/dashboard/permission_group/all';

  constructor(private http: HttpClient) {
  }

  // Get all permission groups
  getPermissionGroups(limit?: number, skip?: number): Observable<AllResponseType<PermissionGroupType>> {
    return this.http.get<AllResponseType<PermissionGroupType>>(
      this.permissionGroupUrl + '/?limit=' + (limit || 20) + '&skip=' + (skip || 0) + '&status=all'
    );
  }
}
