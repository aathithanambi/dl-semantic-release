// Import Core Modules
import {Component, Input} from '@angular/core';
// Import Internal Modules
import {ColumnDefinition} from "../../../core/types/table_type";
import {Router} from "@angular/router";

@Component({
  selector: 'base-table',
  templateUrl: './base-table.component.html',
  styleUrls: ['../../../../scss/_base-table.scss'],
})
export class BaseTableComponent<T extends { _id: string }> {
  @Input()
  entityData?: T[];
  @Input()
  tableTitle?: string;
  @Input()
  private columnDefinition?: ColumnDefinition[];

  get columnNames() {
    return this.columnDefinition?.map((column) => column.name);
  }

  get columns() {
    return this.columnDefinition;
  }

  constructor(private router: Router) {
  }

  detailScreen(id: string) {
    this.navigate('detail', id);
  }

  registerScreen() {
    this.navigate('register');
  }

  private navigate(path: string, id?: string) {
    const currentUrl = this.router.url.split('/');
    currentUrl.pop();
    currentUrl.push(path);
    this.router.navigate(id ? [currentUrl.join('/'), id] : [currentUrl.join('/')]);
  }
}
