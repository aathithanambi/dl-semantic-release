import {NgModule} from '@angular/core';
import {BaseTableComponent} from "./base_table/base-table.component";
import {CommonModule} from "@angular/common";
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    TooltipModule.forRoot(),
    RouterModule
  ],
  exports: [
    BaseTableComponent
  ],
  declarations: [
    BaseTableComponent
  ]
})
export class SharedModule {
}
