// Import Internal Modules
import {ProductCategoryType} from '../product_categories/product_category.type';
import {ProductBrandType} from '../product_brand/product_brand.type';
import {VendorType} from '../../m2_module/vendor/vendor.type';

export class ProductSpecificationType {
  [key: string]: string;
}

export class ProductType {
  _id?: string;
  product_no?: string;
  product_name?: string;
  product_category?: Partial<ProductCategoryType>;
  product_images?: Array<URL>;
  product_description?: string;
  product_overview?: string;
  product_specification?: ProductSpecificationType;
  product_warranty_text?: string;
  /** @todo Change to promotionType after view & detail integration */
  product_promotions?: Array<string>;
  product_brand?: Partial<ProductBrandType>;
  product_vendor?: Partial<VendorType>;
  product_quantity?: number;
  product_selling_price?: number;
  product_discount_percentage?: number;
  product_commission_percentage?: number;
  /** @todo Change to shippingType after view & detail integration */
  product_shipping?: Array<string>;
  product_clicked?: number;
  product_created_date?: Date;
  product_modified_date?: Date;
  product_status_is_active?: boolean;
  entityData?: Array<ProductType>;

  [key: string]: string | number | Date | boolean | Array<string> | Array<URL> | Array<ProductType> | ProductSpecificationType | Partial<VendorType> | Partial<ProductBrandType> | Partial<ProductCategoryType> | undefined

}
