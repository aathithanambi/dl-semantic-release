// Import Angular Core
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {MatProgressBarModule} from "@angular/material/progress-bar";
// Import Modules
import {ProductRoutingModule} from "./product.routing.module";
// Import Components
import {ProductViewComponent} from './product_view/product.view.component';
import {
  ProductDetailComponent,
  SliceSpecificationPipe
} from './product_detail/product.detail.component'
import {SharedModule} from "../../shared/shared.module";
//Import Services
import {ProductService} from './product.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProductRoutingModule,
    SharedModule,
    MatProgressBarModule
  ],
  declarations: [
    ProductViewComponent,
    ProductDetailComponent,
    SliceSpecificationPipe
  ],
  providers: [ProductService]
})
export class ProductModule {
}
