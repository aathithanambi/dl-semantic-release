// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from 'rxjs';
// Import Internal Modules
import {ProductService} from "../product.service";
import {ProductType} from "../product.type";
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'product.view.component.html'
})
export class ProductViewComponent implements OnInit, OnDestroy {
  products?: ProductType[];
  isLoading: boolean = true;
  productSubscription?: Subscription;
  readonly tableTitle = 'Products';
  readonly columnDefinition = [
    {id: 'product_name', name: 'Product Name', type: 'string'},
    {id: 'product_overview', name: 'Product Overview', type: 'string'},
    {id: 'product_description', name: 'Product Description', type: 'string'},
    {id: 'product_created_date', name: 'Product Created Date', type: 'date'}
  ];

  constructor(private readonly router: Router,
              private readonly productService: ProductService,
              private readonly alertService: AlertService,
              ) {
  }

  ngOnInit() {
    this.productSubscription = this.productService.getProducts().subscribe(
      (response) => {
        this.products = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.productSubscription?.unsubscribe()
  }

}
