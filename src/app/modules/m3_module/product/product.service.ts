// Import Core Modules
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// Import Internal Modules
import {ProductType} from "./product.type";
import {environment} from '../../../../environments/environment';
import {AllResponseType} from '../../../core/types/response_type';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  productUrl = environment.apiUrl + 'm3/dashboard/product/all';
  productByIdUrl = environment.apiUrl + 'm3/dashboard/product/search/?id=';
  registerProductUrl = environment.apiUrl + 'm3/dashboard/product/register';
  updateProductUrl = environment.apiUrl + 'm3/dashboard/product/update/';

  constructor(private http: HttpClient) {
  }

  // get all products
  getProducts(): Observable<AllResponseType<ProductType>> {
    return this.http.get<AllResponseType<ProductType>>(this.productUrl);
  }

  // get single product
  getProduct(id: string): Observable<AllResponseType<ProductType>> {
    return this.http.get<AllResponseType<ProductType>>(this.productByIdUrl + id);
  }

  // register product
  registerProduct(data: ProductType): Observable<ProductType> {
    return this.http.post<ProductType>(this.registerProductUrl, data);
  }

  // update product
  updateProduct(id: string, data: ProductType): Observable<ProductType> {
    return this.http.put<ProductType>(this.updateProductUrl + id, data);
  }

}
