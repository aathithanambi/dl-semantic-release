// Import Core Modules
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Import Internal Modules
import {ProductViewComponent} from "./product_view/product.view.component";
import {ProductDetailComponent} from './product_detail/product.detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'Products Home'
    },
    children: [
      {
        path: 'view',
        component: ProductViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: ProductDetailComponent,
        data: {
          title: 'Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
}
