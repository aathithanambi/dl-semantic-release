// Import  Core
import {Component, OnInit, Pipe, PipeTransform, OnDestroy} from '@angular/core'
import {KeyValue, Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
// Import Types
import {ProductType} from '../product.type';
import {ProductService} from '../product.service';
import {AlertService} from '../../../../core/alert/alert.service';

@Component({
  templateUrl: 'product.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  isLoading: boolean = true;
  product?: ProductType;
  productSubscription?: Subscription;
  isActive: boolean = false;

  // product status
  get productStatus(): string {
    return this.isActive ? 'Active' : 'InActive';
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly productService: ProductService,
              private readonly alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.productSubscription = this.productService.getProduct(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.product = response.entityData[0];
        this.isActive = this.product.product_status_is_active!;
        this.isLoading = false;
      }, (error) => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.productSubscription?.unsubscribe();
  }

  // switch toggle
  toggleActive(): void {
    this.isActive = !this.isActive;
  }

  // replace _ character with space character
  getName(value: string): string {
    return value.replace(/_/g, ' ');
  }

  // edit button handler
  edit(): void {
    alert('edit button clicked');
  }

  // back button handler
  back(): void {
    this.location.back();
  }

  // delete button handler
  delete(): void {
    alert('delete button clicked');
  }

}

// Pipe used to slice product specification
@Pipe({name: 'sliceSpecs'})
export class SliceSpecificationPipe implements PipeTransform {
  transform(specification: Array<KeyValue<string, string>>, part: number): Array<KeyValue<string, string>> {
    const specLength = specification.length;
    return part ? specification.slice(specLength / 2) : specification.slice(0, specLength / 2);
  }
}
