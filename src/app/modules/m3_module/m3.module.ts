// Angular Core
import {NgModule} from '@angular/core';
// Import Modules
import {ProductModule} from './product/product.module';
import {ProductCategoryModule} from './product_categories/product_category.module';
import {ProductReviewModule} from './product_review/product_review.module';
import {ProductBrandModule} from './product_brand/product_brand.module';
import {M3RoutingModule} from "./m3.routing.module";

@NgModule({
  imports: [
    M3RoutingModule,
    ProductModule,
    ProductCategoryModule,
    ProductReviewModule,
    ProductBrandModule,
  ]
})
export class M3Module {
}
