// Import Angular Core
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
// Import Modules
import {ProductReviewRoutingModule} from "./product_review.routing.module";
// Import Components
import {ProductReviewViewComponent} from './product_review.view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProductReviewRoutingModule
  ],
  declarations: [
    ProductReviewViewComponent
  ]
})
export class ProductReviewModule {
}
