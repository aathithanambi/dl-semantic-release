// Import Angular Core
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Import Components
import {ProductReviewViewComponent} from "./product_review.view.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'Product Review Home'
    },
    children: [
      {
        path: 'view',
        component: ProductReviewViewComponent,
        data: {
          title: 'View'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductReviewRoutingModule {
}
