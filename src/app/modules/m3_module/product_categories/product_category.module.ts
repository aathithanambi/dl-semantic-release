// Import Core Modules
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MatProgressBarModule } from "@angular/material/progress-bar";
import {NgSelectModule} from "@ng-select/ng-select";
// Import Internal Modules
import { ProductCategoryRoutingModule } from "./product_category.routing.module";
import { ProductCategoryViewComponent } from './product_category_view/product_category.view.component';
import { SharedModule } from "../../shared/shared.module";
import { ProductCategoryDetailComponent } from './product_category_detail/product_category.detail.component';
import { ProductCategoryService } from './product_category.service';
import { ProductCategoryRegisterComponent } from './product_category_register/product_category.register.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressBarModule,
    NgSelectModule,
    ProductCategoryRoutingModule,
    ReactiveFormsModule,
    SharedModule,    
  ],
  declarations: [
    ProductCategoryDetailComponent,
    ProductCategoryRegisterComponent,
    ProductCategoryViewComponent
  ],
  providers: [ProductCategoryService]
})
export class ProductCategoryModule {
}
