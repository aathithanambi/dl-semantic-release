// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
// Import Internal Modules
import {ProductCategoryType} from '../product_category.type';
import {ProductCategoryService} from '../product_category.service';
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'product_category.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class ProductCategoryDetailComponent implements OnInit, OnDestroy {
  productCategoryInformation?: ProductCategoryType;
  isLoading: boolean = true;
  isActive?: boolean = false;
  productCategorySubscription?: Subscription;

  // product category status
  get productCategoryStatus(): string {
    return this.isActive ? 'Active' : 'InActive';
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly productCategoryService: ProductCategoryService,
              private readonly alertService: AlertService
              ) {
  }

  ngOnInit() {
    this.productCategorySubscription = this.productCategoryService.getProductCategory(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.productCategoryInformation = response.entityData[0];
        this.isActive = this.productCategoryInformation.product_category_status_is_active;
        this.isLoading = false;
      }, (error) => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      }
    );
  }

  ngOnDestroy() {
    this.productCategorySubscription?.unsubscribe();
  }

  // switch toggle
  toggleActive(): void {
    this.isActive = !this.isActive;
  }

  // edit button handler
  edit() {
    alert("edit button clicked");
  }

  // back button handler
  back() {
    this.location.back();
  }

  // delete button handler
  delete() {
    alert("delete button clicked");
  }

}
