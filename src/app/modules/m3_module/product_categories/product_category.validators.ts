// Import Core Modules
import { InjectionToken } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
// Import Internal Modules
import { AbstractValidator } from '../../../core/validator/validator';

class ProductCategoryFormValidatorService extends AbstractValidator {
    form?: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        super();
    }

    buildForm() {
        this.prepareProductCategoryForm();
        return this.form!;
    }

    // prepare product register form

    private prepareProductCategoryForm() {
        this.form = this.formBuilder.group({
            product_category_name: ["", [Validators.required, Validators.pattern(/(^[a-zA-Z0-9])+([\sa-zA-Z0-9,.()"&/'\\*!#_=+?:;-])*$/), Validators.minLength(3), Validators.maxLength(50)]],
            product_category_images: ["", Validators.required],
            product_category_overview: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(500), Validators.pattern(/^[a-zA-Z0-9]+.*$/)]],
            product_category_description: ["", [Validators.required, Validators.minLength(10), Validators.maxLength(2000), Validators.pattern(/(^[a-zA-Z0-9])+.*$/)]],
            product_category_children: ["", Validators.required],
            product_category_parent: ["", Validators.required],
            product_category_has_sub_category:[]
        })
    }

}

export const PRODUCT_CATEGORY_FORM_VALIDATOR_TOKEN = new InjectionToken<ProductCategoryFormValidatorService>('ProductCategoryFormValidatorService', {
    providedIn: 'root',
    factory: () => new ProductCategoryFormValidatorService(new FormBuilder())
});
