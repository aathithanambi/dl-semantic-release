//Import Core Modules
import {Component, Inject} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {Router} from "@angular/router";
import {takeUntil} from "rxjs/operators";
import {forkJoin} from "rxjs";
//Import Internal Modules
import {ProductCategoryService} from '../product_category.service';
import {AlertService} from '../../../../core/alert/alert.service';
import {AbstractValidator} from '../../../../core/validator/validator';
import {ProductCategoryType} from "../product_category.type";
import {PRODUCT_CATEGORY_FORM_VALIDATOR_TOKEN} from '../product_category.validators';
import {MetadataService} from '../../../m0_module/metadata/metadata.service';
import {AbstractRegister} from '../../../../core/modules/register';

@Component({
  templateUrl: 'product_category.register.component.html',
  styleUrls: ['../../../../../scss/_registers.scss'],
})
export class ProductCategoryRegisterComponent extends AbstractRegister<ProductCategoryType> {
  registerForm?: FormGroup;
  fileNames: string[] = [];
  _productCategories?: any;
  productCategories?: any[];
  sequence: string = 'Error';
  isProductCategoryImageFieldTouched = false;
  imageUrls: any[] = [];
  hasSubCategory: boolean = false;

  constructor(private router: Router,
              private location: Location,
              private productCategoryService: ProductCategoryService,
              private alertService: AlertService,
              @Inject(PRODUCT_CATEGORY_FORM_VALIDATOR_TOKEN) private productCategoryRegisterValidator: AbstractValidator,
              metadata: MetadataService) {
    super();
    this.showProgressBar(true);

    // Load Register Form data
    forkJoin({
      metadata: metadata.getMetadata('product_categories'),
      productCategories: productCategoryService.getProductCategories()
    }).pipe(takeUntil(this.destroy$)).subscribe((response) => {
      this.sequence = response.metadata.sequence;
      this._productCategories = response.productCategories.entityData;
      this.productCategories = this._productCategories.map((productCategory: ProductCategoryType) => productCategory.product_category_name)
    }, (error) => {
      this.alertService.error(error.error);
      this.router.navigateByUrl('m3/product_category/view');
    }).add(() => this.showProgressBar(false));
  }

  ngOnInit() {
    this.registerForm = this.productCategoryRegisterValidator.buildForm();
  }

  toogleCategory() {
    this.hasSubCategory = !this.hasSubCategory;
  }

  // trigger the click event
  selectFile(inputEle: HTMLInputElement) {
    this.isProductCategoryImageFieldTouched = true;
    inputEle.click();
  }

  // returns the info text
  get infoText() {
    return this.imageUrls.length > 0 ? `${this.imageUrls.length} file(s) selected` : 'No file choosen';
  }

  // remove image
  removeImage(imgName: string) {
    this.imageUrls = this.imageUrls.filter(name => name !== imgName);
  }

  // select images
  onFileSelect(event: any) {
    let validExtenstions = ['png', 'jpg', 'jpeg', 'gif'];
    if (event.target.files.length > 0) {
      const files = event.target.files;
      for (let i = 0; i < files.length; i++) {
        let fileExt = files[i].name.split('.')[1];
        if (validExtenstions.includes(fileExt)) {
          this.imageUrls.push(files[i].name);
        }
      }
    }
    let imageSet = new Set(this.imageUrls)
    this.imageUrls = Array.from(imageSet);
    if (this.imageUrls.length > 0) {
      this.isProductCategoryImageFieldTouched = false;
      this.registerForm?.get('product_category_images')?.setValue(this.imageUrls.map(name => `https://images-na.ssl-images-amazon.com/images/I/${name}`));
    }
  }

  // form submit handler
  submit(): void {
    this.registerForm?.get('product_category_has_sub_category')?.setValue(this.hasSubCategory);
    let productCategoryParentValue = this.registerForm?.get('product_category_parent')?.value;
    let productCategoryChildValue = this.registerForm?.get('product_category_children')?.value;
    let productCategoryChildrens: any[] = [];
    let parentValue = this._productCategories.find((productCategory: ProductCategoryType) => productCategory.product_category_name === productCategoryParentValue);
    productCategoryChildValue.forEach((childValue: ProductCategoryType) => {
      let value = this._productCategories.find((item: ProductCategoryType) => {
        return item.product_category_name === childValue.product_category_name;
      });
      productCategoryChildrens.push({_id: {id: value._id}, product_category_name: value.product_category_name})
    });
    const productCategoryFormData = {
      ...this.registerForm?.value,
      product_category_parent: {_id: {id: parentValue._id}, product_category_name: parentValue.product_category_name},
      product_category_children: productCategoryChildrens
    };
    this.showProgressBar(true, 'Registering New Product Category');
    this.productCategoryService.registerProductCategory(productCategoryFormData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (productCategory: ProductCategoryType) => {
          this.alertService.success(`Thank you, Added ${productCategory.product_category_name}`);
          this.registerForm?.reset();
          this.router.navigateByUrl('m3/product_category/view');
        },
        (error) => {
          this.alertService.error(error.error);
        },
      ).add(() => this.showProgressBar(false));
  }

  // back button handler
  back(): void {
    this.location.back();
  }

}
