export class ProductCategoryType {
    _id?: string;
    product_category_no?: string;
    product_category_name?: string;
    product_category_images?: Array<string>;
    product_category_overview?: string;
    product_category_description?: string;
    product_category_has_sub_category?: boolean;
    product_category_children?: Array<Partial<ProductCategoryType>>;
    product_category_parent?: Partial<ProductCategoryType>;
    product_category_created_date?: Date;
    product_category_modified_date?: Date;
    product_category_status_is_active?: boolean;

    [key:string] : string | Array<string> | boolean | Date | Partial<ProductCategoryType> |Array<Partial<ProductCategoryType>> | undefined;
}
