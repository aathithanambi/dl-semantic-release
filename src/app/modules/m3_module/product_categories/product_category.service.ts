// Import Core Modules
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// Import Internal Modules
import {ProductCategoryType} from "./product_category.type";
import {environment} from '../../../../environments/environment';
import {AllResponseType} from '../../../core/types/response_type';

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryService {
  productCategoryUrl = environment.apiUrl + 'm3/dashboard/product_category/all';
  productCategoryByIdUrl = environment.apiUrl + 'm3/dashboard/product_category/search/?id=';
  productCategoryRegisterUrl = environment.apiUrl + 'm3/dashboard/product_category/register';
  productCategoryUpdateUrl = environment.apiUrl + 'm3/dashboard/product_category/update/';

  constructor(private http: HttpClient) {
  }

  // get all product categories
  getProductCategories(): Observable<AllResponseType<ProductCategoryType>> {
    return this.http.get<AllResponseType<ProductCategoryType>>(this.productCategoryUrl);
  }

  // get single product category
  getProductCategory(id: string): Observable<AllResponseType<ProductCategoryType>> {
    return this.http.get<AllResponseType<ProductCategoryType>>(this.productCategoryByIdUrl + id);
  }

  // register product category
  registerProductCategory(data: ProductCategoryType): Observable<ProductCategoryType> {
    return this.http.post<ProductCategoryType>(this.productCategoryRegisterUrl, data);
  }

  // update product category
  updateProductCategory(id: string, data: ProductCategoryType): Observable<ProductCategoryType> {
    return this.http.put<ProductCategoryType>(this.productCategoryUpdateUrl + id, data);
  }

}
