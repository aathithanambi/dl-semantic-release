// Import Core Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import Internal Modules
import { ProductCategoryViewComponent } from "./product_category_view/product_category.view.component";
import { ProductCategoryDetailComponent } from './product_category_detail/product_category.detail.component'
import { ProductCategoryRegisterComponent } from './product_category_register/product_category.register.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full',
  },  
  {
    path: '',
    data: {
      title: 'Product Category Home'
    },
    children: [
      {
        path: 'view',
        component: ProductCategoryViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: ProductCategoryDetailComponent,
        data: {
          title: 'Detail'
        }
      },
      {
        path: 'register',
        component: ProductCategoryRegisterComponent,
        data: {
          title: 'Register'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductCategoryRoutingModule {
}
