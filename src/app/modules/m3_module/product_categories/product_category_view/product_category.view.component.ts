// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from 'rxjs';
// Import Internal Modules
import {ProductCategoryService} from "../product_category.service";
import {ProductCategoryType} from "../product_category.type";
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'product_category.view.component.html'
})
export class ProductCategoryViewComponent implements OnInit, OnDestroy {
  productCategories?: ProductCategoryType[];
  isLoading: boolean = true;
  productCategorySubscription?: Subscription;
  readonly tableTitle = 'Product Categories';
  readonly columnDefinition = [
    {id: 'product_category_name', name: 'Product Category Name', type: 'string'},
    {id: 'product_category_overview', name: 'Product Category Overview', type: 'string'},
    {id: 'product_category_description', name: 'Product Category Description', type: 'string'},
    {id: 'product_category_created_date', name: 'Product Category Created Date', type: 'date'}
  ];

  get productCategory(): ProductCategoryType[] {
    this.productCategoryService.getProductCategories().subscribe(productCategory => {
      this.productCategories = productCategory.entityData;
    });
    return this.productCategories || [];
  }

  constructor(private readonly router: Router,
              private readonly productCategoryService: ProductCategoryService,
              private readonly alertService: AlertService) {
  }

  ngOnInit() {
    this.productCategorySubscription = this.productCategoryService.getProductCategories().subscribe(
      (response) => {
        this.productCategories = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this?.productCategorySubscription?.unsubscribe();
  }
}
