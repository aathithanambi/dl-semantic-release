// Import Angular Core
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'product',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'M3 Module'
    },
    children: [
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'product_category',
        loadChildren: () => import('./product_categories/product_category.module').then(m => m.ProductCategoryModule)
      },
      {
        path: 'product_review',
        loadChildren: () => import('./product_review/product_review.module').then(m => m.ProductReviewModule)
      },
      {
        path: 'product_brand',
        loadChildren: () => import('./product_brand/product_brand.module').then(m => m.ProductBrandModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class M3RoutingModule {
}
