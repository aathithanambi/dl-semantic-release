// Import Angular Core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import Components
import { ProductBrandViewComponent } from "./product_brand_view/product_brand.view.component";
import { ProductBrandDetailComponent } from './product_brand_detail/product_brand.detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'Product Brand Home'
    },
    children: [
      {
        path: 'view',
        component: ProductBrandViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: ProductBrandDetailComponent,
        data: {
          title: 'Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductBrandRoutingModule {
}
