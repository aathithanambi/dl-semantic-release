export class ProductBrandType{
  _id?: string;
  product_brand_no?: string;
  product_brand_name?: string;
  product_brand_image?: string;
  product_brand_created_date?: Date;
  product_brand_modified_date?: Date;
  product_brand_status_is_active?: boolean;

  [key:string] : string | boolean | Date | undefined
}
