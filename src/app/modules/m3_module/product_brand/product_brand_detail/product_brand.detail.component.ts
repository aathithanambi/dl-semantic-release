// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
// Import Internal Modules
import {ProductBrandType} from '../product_brand.type';
import {ProductBrandService} from '../product_brand.service';
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'product_brand.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class ProductBrandDetailComponent implements OnInit, OnDestroy {
  isLoading: boolean = true;
  productBrandInformation?: ProductBrandType;
  productBrandSubscription?: Subscription;
  isActive: boolean = false;

  // product status
  get productBrandStatus(): string {
    return this.isActive ? 'Active' : 'InActive';
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly productBrandService: ProductBrandService,
              private readonly alertService: AlertService
              ) {
  }

  ngOnInit() {
    this.productBrandService.getProductBrand(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.productBrandInformation = response.entityData[0];
        this.isActive = this.productBrandInformation.product_brand_status_is_active!;
        this.isLoading = false;
      }, (error) => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.productBrandSubscription?.unsubscribe();
  }

  // switch toggle
  toggleActive(): void {
    this.isActive = !this.isActive;
  }

  // edit button handler
  edit(): void {
    alert('edit button clicked');
  }

  // back button handler
  back(): void {
    this.location.back();
  }

  // delete button handler
  delete(): void {
    alert('delete button clicked');
  }
}
