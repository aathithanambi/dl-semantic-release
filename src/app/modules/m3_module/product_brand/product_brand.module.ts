// Import Core
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {MatProgressBarModule} from "@angular/material/progress-bar";
// Import Modules
import { ProductBrandRoutingModule } from "./product_brand.routing.module";
// Import Components
import { ProductBrandViewComponent } from './product_brand_view/product_brand.view.component';
import { SharedModule } from '../../shared/shared.module';
import { ProductBrandDetailComponent } from './product_brand_detail/product_brand.detail.component'
// Import Services
import { ProductBrandService } from './product_brand.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProductBrandRoutingModule,
    SharedModule,
    MatProgressBarModule
  ],
  declarations: [
    ProductBrandViewComponent,
    ProductBrandDetailComponent
  ],
  providers:[ProductBrandService]
})
export class ProductBrandModule {
}
