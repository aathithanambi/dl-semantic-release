// Import Angular Core
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// Import Internal Modules
import {environment} from '../../../../environments/environment';
import {ProductBrandType} from './product_brand.type';
import {AllResponseType} from '../../../core/types/response_type';

@Injectable({
  providedIn: 'root',
})
export class ProductBrandService {
  productBrandUrl = environment.apiUrl + 'm3/dashboard/product_brand/all';
  productBrandByUrl = environment.apiUrl + 'm3/dashboard/product_brand/search/?id=';
  productBrandRegisterUrl = environment.apiUrl + 'm3/dashboard/product_brand/register';
  productBrandUpdateUrl = environment.apiUrl + 'm3/dashboard/product_brand/update/';

  constructor(private http: HttpClient) {
  }

  // get all product brands
  getProductBrands(): Observable<AllResponseType<ProductBrandType>> {
    return this.http.get<AllResponseType<ProductBrandType>>(this.productBrandUrl);
  }

  // get single product brand
  getProductBrand(id: string): Observable<AllResponseType<ProductBrandType>> {
    return this.http.get<AllResponseType<ProductBrandType>>(this.productBrandByUrl + id);
  }

  // register product brand
  registerProductBrand(data: ProductBrandType): Observable<ProductBrandType> {
    return this.http.post<ProductBrandType>(this.productBrandRegisterUrl, data);
  }

  // update product brand
  updateProductBrand(id: string, data: ProductBrandType): Observable<ProductBrandType> {
    return this.http.put<ProductBrandType>(this.productBrandUpdateUrl + id, data);
  }

}
