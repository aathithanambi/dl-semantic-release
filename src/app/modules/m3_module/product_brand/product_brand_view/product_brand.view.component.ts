// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from 'rxjs';
// Import Internal Modules
import {ProductBrandService} from "../product_brand.service";
import {ProductBrandType} from "../product_brand.type";
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'product_brand.view.component.html'
})
export class ProductBrandViewComponent implements OnInit, OnDestroy {
  productBrands?: ProductBrandType[];
  isLoading: boolean = true;
  productBrandSubscription?: Subscription;
  readonly tableTitle = 'Product Brands';
  readonly columnDefinition = [
    {id: 'product_brand_name', name: 'Product Brand Name', type: 'string'},
    {id: 'product_brand_created_date', name: 'Product Brand Created Date', type: 'date'}
  ];

  constructor(private readonly router: Router,
              private readonly productBrandService: ProductBrandService,
              private readonly alertService: AlertService,) {
  }

  ngOnInit() {
    this.productBrandSubscription = this.productBrandService.getProductBrands().subscribe(
      (response) => {
        this.productBrands = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.productBrandSubscription?.unsubscribe();
  }

}
