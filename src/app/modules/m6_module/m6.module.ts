// Angular Core
import {NgModule} from '@angular/core';
// Import Modules
import {PromotionModule} from './promotion/promotion.module';
import {PromotionCategoryModule} from './promotion_category/promotion_category.module';
import {M6RoutingModule} from "./m6.routing.module";

@NgModule({
  imports: [
    PromotionModule,
    PromotionCategoryModule,
    M6RoutingModule
  ]
})
export class M6Module {
}
