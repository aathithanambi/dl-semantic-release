// Import Angular Core
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'promotion',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'M6 Module'
    },
    children: [
      {
        path: 'promotion',
        loadChildren: () => import('./promotion/promotion.module').then(m => m.PromotionModule)
      },
      {
        path: 'promotion_category',
        loadChildren: () => import('./promotion_category/promotion_category.module').then(m => m.PromotionCategoryModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class M6RoutingModule {
}
