// Import External Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
// Import Internal Modules
import {PromotionService} from '../promotion.service';
import {PromotionType} from '../promotion.type';
import {AlertService} from '../../../../core/alert/alert.service';

@Component({
  templateUrl: 'promotion.view.component.html'
})
export class PromotionViewComponent implements OnInit, OnDestroy {
  promotions?: PromotionType[];
  isLoading: boolean = true;
  promotionSubscription?: Subscription;
  readonly tableTitle = 'Promotions';
  readonly columnDefinition = [
    {id: 'promotion_title', name: 'Promotion Title', type: 'string'},
    {id: 'promotion_created_date', name: 'Promotion Created Date', type: 'date'}
  ];

  constructor(private readonly router: Router,
              private readonly promotionService: PromotionService,
              private readonly alertService: AlertService,
              ) {
  }

  ngOnInit() {
    this.promotionSubscription = this.promotionService.getPromotions().subscribe(
      (response) => {
        this.promotions = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.promotionSubscription?.unsubscribe();
  }

}
