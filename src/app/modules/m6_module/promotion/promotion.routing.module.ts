// Import Core Modules
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Import Internal Modules
import {PromotionViewComponent} from "./promotion_view/promotion.view.component";
import { PromotionDetailComponent } from "./promotion_detail/promotion.detail.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'Promotion Home'
    },
    children: [
      {
        path: 'view',
        component: PromotionViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: PromotionDetailComponent,
        data: {
          title: 'Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionRoutingModule {}
