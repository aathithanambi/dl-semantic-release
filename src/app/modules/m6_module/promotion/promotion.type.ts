export class PromotionType {
  _id?: string;
  promotion_no?: string;
  promotion_product_id?: string;
  promotion_title?: string;
  promotion_category?: Array<string>;
  promotion_image?: string;
  promotion_created_date?: Date;
  promotion_start_date?: Date;
  promotion_expire_date?: boolean;
  promotion_clicked?: number;
  promotion_modified_date?: Date;
  promotion_status_is_active?: boolean;

  [key:string] : string | Array<string> | number | boolean | Date | undefined
}
