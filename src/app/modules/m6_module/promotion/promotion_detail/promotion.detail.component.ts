// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Location} from '@angular/common';
// Import Internal Modules
import {PromotionType} from '../promotion.type';
import {PromotionService} from '../promotion.service';
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'promotion.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class PromotionDetailComponent implements OnInit, OnDestroy {
  isLoading: boolean = true;
  promotion?: PromotionType;
  promotionSubscription?: Subscription;
  isActive: boolean = false;

  // promotion status
  get promotionStatus(): string {
    return this.isActive ? "Active" : "InActive";
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly promotionService: PromotionService,
              private readonly alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.promotionSubscription = this.promotionService.getPromotion(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.promotion = response.entityData[0];
        this.isActive = this.promotion.promotion_status_is_active!;
        this.isLoading = false;
      }, (error) => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.promotionSubscription?.unsubscribe();
  }

  // switch toggle
  toggleActive(): void {
    this.isActive = !this.isActive;
  }

  // edit button handler
  edit(): void {
    alert('edit button clicked');
  }

  // back button handler
  back(): void {
    this.location.back();
  }

  // delete button handler
  delete(): void {
    alert('delete button clicked');
  }
}
