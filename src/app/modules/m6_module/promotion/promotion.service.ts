// Import Core Modules
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
// Import Internal Modules
import {PromotionType} from './promotion.type';
import {environment} from '../../../../environments/environment';
import {AllResponseType} from '../../../core/types/response_type';

@Injectable({
  providedIn: 'root',
})
export class PromotionService {
  promotionUrl = environment.apiUrl + 'm6/dashboard/promotion/all';
  promotionByIdUrl = environment.apiUrl + 'm6/dashboard/promotion/search/?id=';
  promotionRegisterUrl = environment.apiUrl + 'm6/dashboard/promotion/register';
  promotionUpdateUrl = environment.apiUrl + 'm6/dashboard/promotion/update/';

  constructor(private http: HttpClient) {
  }

  // get all promotions
  getPromotions(): Observable<AllResponseType<PromotionType>> {
    return this.http.get<AllResponseType<PromotionType>>(this.promotionUrl);
  }

  // get single promotion
  getPromotion(id: string): Observable<AllResponseType<PromotionType>> {
    return this.http.get<AllResponseType<PromotionType>>(this.promotionByIdUrl + id);
  }

  // register promotion
  registerPromotion(data: PromotionType): Observable<PromotionType> {
    return this.http.post<PromotionType>(this.promotionRegisterUrl, data);
  }

  // update promotion
  updatePromotion(id: string, data: PromotionType): Observable<PromotionType> {
    return this.http.put<PromotionType>(this.promotionUpdateUrl + id, data);
  }

}
