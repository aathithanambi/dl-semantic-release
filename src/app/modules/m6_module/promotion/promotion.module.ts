// Import Core Modules
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {MatProgressBarModule} from "@angular/material/progress-bar";
// Import Internal Modules
import {PromotionRoutingModule} from "./promotion.routing.module";
import {PromotionViewComponent} from './promotion_view/promotion.view.component';
import {PromotionDetailComponent} from "./promotion_detail/promotion.detail.component";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressBarModule,
    PromotionRoutingModule,
    SharedModule
  ],
  declarations: [
    PromotionDetailComponent,
    PromotionViewComponent
  ]
})
export class PromotionModule {
}
