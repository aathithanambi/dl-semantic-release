// Import Core Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import Internal Modules
import { PromotionCategoryViewComponent } from "./promotion_category_view/promotion_category.view.component";
import { PromotionCategoryDetailComponent } from './promotion_category_detail/promotion_category.detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'Promotion Category Home'
    },
    children: [
      {
        path: 'view',
        component: PromotionCategoryViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: PromotionCategoryDetailComponent,
        data: {
          title: 'Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionCategoryRoutingModule {
}
