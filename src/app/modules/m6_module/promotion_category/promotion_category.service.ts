// Import Angular Core
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// Import Internal Modules
import {PromotionCategoryType} from "./promotion_category.type";
import {environment} from '../../../../environments/environment';
import {AllResponseType} from '../../../core/types/response_type';

@Injectable({
  providedIn: 'root',
})
export class PromotionCategoryService {
  promotionCategoryUrl = environment.apiUrl + 'm6/dashboard/promotion_category/all';
  promotionCategoryByIdUrl = environment.apiUrl + 'm6/dashboard/promotion_category/search/?id=';
  promotionCategoryRegisterUrl = environment.apiUrl + 'm6/dashboard/promotion_category/register';
  promotionCategoryUpdateUrl = environment.apiUrl + 'm6/dashboard/promotion_category/update/';

  constructor(private http: HttpClient) {
  }

  // get all promotion categories
  getPromotionCategories(): Observable<AllResponseType<PromotionCategoryType>> {
    return this.http.get<AllResponseType<PromotionCategoryType>>(this.promotionCategoryUrl);
  }

  // get single promotion category
  getPromotionCategory(id: string): Observable<AllResponseType<PromotionCategoryType>> {
    return this.http.get<AllResponseType<PromotionCategoryType>>(this.promotionCategoryByIdUrl + id);
  }

  // register promotion category
  registerPromotionCategory(data: PromotionCategoryType): Observable<PromotionCategoryType> {
    return this.http.post<PromotionCategoryType>(this.promotionCategoryRegisterUrl, data);
  }

  // update promotion category
  updatePromotionCategory(id: string, data: PromotionCategoryType): Observable<PromotionCategoryType> {
    return this.http.put<PromotionCategoryType>(this.promotionCategoryUpdateUrl + id, data);
  }
}
