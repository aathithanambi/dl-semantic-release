// Import External Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
// Import Internal Modules
import {PromotionCategoryService} from '../promotion_category.service';
import {PromotionCategoryType} from '../promotion_category.type';
import {AlertService} from '../../../../core/alert/alert.service';

@Component({
  templateUrl: 'promotion_category.view.component.html'
})
export class PromotionCategoryViewComponent implements OnInit, OnDestroy {
  promotionsCategories?: PromotionCategoryType[];
  isLoading: boolean = true;
  promotionCategorySubscription?: Subscription;
  readonly tableTitle = 'Promotion Categories';
  readonly columnDefinition = [
    {id: 'promotion_category_name', name: 'Promotion Category Name', type: 'string'},
    {id: 'promotion_category_overview', name: 'Promotion Category Overview', type: 'string'},
    {id: 'promotion_category_created_date', name: 'Promotion Category Created Date', type: 'date'}
  ];

  constructor(private router: Router,
              private promotionCategoryService: PromotionCategoryService,
              private alertService: AlertService,
  ) {
  }

  ngOnInit() {
    this.promotionCategorySubscription = this.promotionCategoryService.getPromotionCategories().subscribe(
      (response) => {
        this.promotionsCategories = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.promotionCategorySubscription?.unsubscribe();
  }
}
