export class PromotionCategoryType {
  _id?: string;
  promotion_category_no?: string;
  promotion_category_name?: string;
  promotion_category_overview?: string;
  promotion_category_description?: string;
  promotion_category_has_sub_category?: boolean;
  promotion_category_parent?: Partial<PromotionCategoryType>;
  promotion_category_children?: Array<Partial<PromotionCategoryType>>;
  promotion_category_created_date?: Date;
  promotion_category_modified_date?: Date;
  promotion_category_status_is_active?: boolean;

  [key:string] : string | boolean | Partial<PromotionCategoryType> |  Array<Partial<PromotionCategoryType>> | Date | undefined;

}

