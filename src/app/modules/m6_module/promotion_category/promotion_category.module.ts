// Import Core Modules
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {MatProgressBarModule} from "@angular/material/progress-bar";
// Import Internal Modules
import {PromotionCategoryRoutingModule} from "./promotion_category.routing.module";
import {PromotionCategoryViewComponent} from './promotion_category_view/promotion_category.view.component';
import {SharedModule} from "../../shared/shared.module";
import {PromotionCategoryDetailComponent} from './promotion_category_detail/promotion_category.detail.component'
import {PromotionCategoryService} from './promotion_category.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressBarModule,
    PromotionCategoryRoutingModule,
    SharedModule
  ],
  declarations: [
    PromotionCategoryDetailComponent,
    PromotionCategoryViewComponent
  ],
  providers: [PromotionCategoryService]
})
export class PromotionCategoryModule {
}
