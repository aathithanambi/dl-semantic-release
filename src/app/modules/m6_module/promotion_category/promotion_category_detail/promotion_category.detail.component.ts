// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
// Import Internal Modules
import {PromotionCategoryType} from '../promotion_category.type';
import {PromotionCategoryService} from '../promotion_category.service';
import {AlertService} from '../../../../core/alert/alert.service';

@Component({
  templateUrl: 'promotion_category.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class PromotionCategoryDetailComponent implements OnInit, OnDestroy {
  isLoading = true;
  promotionCategory?: PromotionCategoryType;
  isActive?: boolean = false;
  promotionCategorySubscription?: Subscription;

  // promotion category status
  get promotionCategoryStatus() {
    return this.isActive ? 'Active' : 'InActive';
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly promotionCategoryService: PromotionCategoryService,
              private readonly alertService: AlertService) {
  }

  ngOnInit() {
    this.promotionCategorySubscription = this.promotionCategoryService.getPromotionCategory(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.promotionCategory = response.entityData[0];
        this.isActive = this.promotionCategory.promotion_category_status_is_active;
        this.isLoading = false;
      }, (error) => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.promotionCategorySubscription?.unsubscribe();
  }

  // toogle status
  toggleActive() {
    this.isActive = !this.isActive;
  }

  // edit button handler
  edit(): void {
    alert('edit button clicked');
  }

  // back button handler
  back(): void {
    this.location.back();
  }

  // delete button  handler
  delete(): void {
    alert('delete button clicked');
  }

}
