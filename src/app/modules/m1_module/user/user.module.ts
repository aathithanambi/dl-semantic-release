// Import Angular Core
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {MatProgressBarModule} from "@angular/material/progress-bar";
// Import Modules
import {UserRoutingModule} from "./user.routing.module";
import {SharedModule} from "../../shared/shared.module";
import {CoreModule} from "../../../core/core.module";
// Import Components
import {UserViewComponent} from './user_view/user.view.component'
import {UserDetailComponent} from './user_detail/user.detail.component'
import {UserService} from './user.service';
import {UserLoginComponent} from "./user_login/user.login.component";
import {UserRegisterComponent} from './user_register/user.register.component';
import {NgSelectModule} from "@ng-select/ng-select";

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    MatProgressBarModule,
    NgSelectModule,
    ReactiveFormsModule,
    SharedModule,
    UserRoutingModule,
  ],
  declarations: [
    UserDetailComponent,
    UserLoginComponent,
    UserRegisterComponent,
    UserViewComponent,
  ],
  providers: [UserService],
  exports: [UserLoginComponent]
})
export class UserModule {
}
