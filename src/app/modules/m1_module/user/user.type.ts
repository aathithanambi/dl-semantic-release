// Import Internal Modules
import {PermissionGroupType} from "../../m0_module/permission_group/permission_group.type";

export class PasswordChangeRequestType {
  key?: string;
  creation_time?: Date;
  status?: 'success' | 'failure';
}

export class UserType {
  _id?: string;
  user_no?: string;
  user_full_name?: string;
  user_email?: string;
  user_password?: string;
  user_phone?: string;
  user_visits?: number;
  user_password_change_request?: PasswordChangeRequestType;
  user_permission_group?: Partial<PermissionGroupType>;
  user_status_is_active?: boolean;
  user_last_logged_in?: Date;
  user_created_date?: Date;
  user_modified_date?: Date;
  user_token?: string;
  entityData?: Array<UserType>;
  token?: string;

  [key: string]: string | number | boolean | Date | PasswordChangeRequestType | Partial<PermissionGroupType> | Array<UserType> | undefined;
}
