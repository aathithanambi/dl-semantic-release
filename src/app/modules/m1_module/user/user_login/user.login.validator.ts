// Import Core Modules
import {InjectionToken} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
// Import Internal Modules
import {AbstractValidator} from '../../../../core/validator/validator';

class UserLoginValidatorService extends AbstractValidator {

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  buildForm(): FormGroup {
    return this.formBuilder.group({
      user_email: [
        '',
        [
          Validators.required,
          Validators.pattern(
            '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'
          ),
          this.patternValidator(/^(\d)(?!\1+$)\d{9}$/, {
            hasMobileNumber: true
          })
        ]
      ],
      user_password: [
        '',
        [
          // 1. Password Field is Required
          Validators.required,
          // 2. Has a minimum length of 8 characters
          Validators.minLength(8),
          Validators.maxLength(26)
        ]
      ]
    });
  }
}

export const USER_LOGIN_VALIDATOR_TOKEN = new InjectionToken<UserLoginValidatorService>('UserLoginValidatorService', {
  providedIn: 'root',
  factory: () => new UserLoginValidatorService(new FormBuilder())
});
