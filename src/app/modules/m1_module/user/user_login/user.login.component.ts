// Import Core Modules
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {
  FormGroup,
  FormControl
} from '@angular/forms';
import {Router} from '@angular/router';
import {Subscription} from "rxjs";
// Import Internal Modules
import {UserService} from '../user.service';
import {USER_LOGIN_VALIDATOR_TOKEN} from './user.login.validator';
import {AlertService} from "../../../../core/alert/alert.service";
import {AbstractValidator} from "../../../../core/validator/validator";

@Component({
  selector: 'user-login',
  templateUrl: 'user.login.component.html',
  styleUrls: ['./user.login.component.scss']
})
export class UserLoginComponent implements OnInit, OnDestroy {
  private loginForm?: FormGroup;
  submitted = false;
  inValidUser = false;
  private userServiceSubscription?: Subscription;

  // convenience getter for easy access to form fields
  get f() {
    return this?.loginForm?.controls;
  }

  constructor(private router: Router,
              private userService: UserService,
              @Inject(USER_LOGIN_VALIDATOR_TOKEN) private userLoginValidator: AbstractValidator,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.userService.logoutUser();
    this.loginForm = this.userLoginValidator.buildForm();
  }

  ngOnDestroy() {
    this?.userServiceSubscription?.unsubscribe();
  }

  onSubmit(userLoginForm: HTMLFormElement) {
    this.submitted = true;
    if (this?.loginForm?.errors) {
      return;
    }

    // if user entered phone number
    if (this.f?.user_email.errors?.email) {
      this.loginForm?.addControl(
        'userPhone',
        new FormControl(this.loginForm.value.user_email)
      );
      delete this?.loginForm?.value.user_email;
    }

    this.alertService.success('Login Success');


    this.userServiceSubscription = this.userService.loginUser(this?.loginForm?.value).subscribe(
      data => {
        this.submitted = false;
        userLoginForm.reset();
        this.alertService.success('Welcome ' + data.user_full_name + '.');
        this.router.navigateByUrl('/')
      },
      () => {
        this.submitted = false;
        this.inValidUser = true;
        userLoginForm.reset();
      }
    );
  }
}
