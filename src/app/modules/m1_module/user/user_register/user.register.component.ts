// Import Core Modules
import {Component, Inject} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {Router} from "@angular/router";
import {takeUntil} from "rxjs/operators";
import {forkJoin} from "rxjs";
// Import Internal Modules
import {UserService} from '../user.service';
import {AlertService} from '../../../../core/alert/alert.service';
import {USER_FORM_VALIDATOR_TOKEN} from '../user.validators';
import {AbstractValidator} from "../../../../core/validator/validator";
import {AbstractRegister} from "../../../../core/modules/register";
import {UserType} from "../user.type";
import {PermissionGroupService} from "../../../m0_module/permission_group/permission_group.service";
import {MetadataService} from "../../../m0_module/metadata/metadata.service";

@Component({
  templateUrl: 'user.register.component.html',
  styleUrls: ['../../../../../scss/_registers.scss'],
})
export class UserRegisterComponent extends AbstractRegister<UserType> {
  registerForm?: FormGroup;
  private userRegisterFields = [
    'user_full_name',
    'user_email',
    'user_password',
    'user_phone',
  ];
  addressTypes = ['Address Type 1', 'Address Type 2'];
  permissionGroups: string[] = [];
  sequence: string = 'Error';

  constructor(private router: Router,
              private location: Location,
              private userService: UserService,
              private alertService: AlertService,
              @Inject(USER_FORM_VALIDATOR_TOKEN) private userRegisterValidator: AbstractValidator,
              permissionGroupService: PermissionGroupService,
              metadata: MetadataService) {
    super();
    this.showProgressBar(true);
    // Load Register Form data
    forkJoin({
      permissionGroups: permissionGroupService.getPermissionGroups(),
      metadata: metadata.getMetadata('users')
    }).pipe(takeUntil(this.destroy$)).subscribe((response) => {
      this.permissionGroups = response.permissionGroups.entityData.map((permissionGroup) => permissionGroup.permission_group_name!);
      this.sequence = response.metadata.sequence;
    }, (error) => {
      this.alertService.error(error.error);
      this.router.navigateByUrl('m1/user/view');
    }).add(() => this.showProgressBar(false));
  }

  // get the registerForm
  ngOnInit() {
    this.registerForm = this.userRegisterValidator.buildForm();
  }

  // form submit handler
  submit(): void {
    const userFormData = this.registerForm?.value;
    let user = this.getFormData(userFormData, this.userRegisterFields);
    this.showProgressBar(true, 'Registering New User');
    this.userService.registerUser(user)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (user) => {
          this.alertService.success(`Thank you ${user.user_full_name}`);
          this.registerForm?.reset();
          this.router.navigateByUrl('m1/user/view');
        },
        (error) => {
          this.registerForm?.get('user_password')?.setValue('');
          this.registerForm?.get('user_confirm_password')?.setValue('');
          this.alertService.error(error.error);
        },
      ).add(() => this.showProgressBar(false));
  }

  // back button handler
  back(): void {
    this.location.back();
  }
}
