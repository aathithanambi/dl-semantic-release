// Import Core Modules
import {InjectionToken} from "@angular/core";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
// Import Internal Modules
import {AbstractValidator} from '../../../core/validator/validator';

class UserFormValidatorService extends AbstractValidator {
  form?: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  buildForm(formType?: 'edit') {
    this.prepareUserRegisterForm();
    if (formType === 'edit') {
      this.prepareUserEditForm();
    }
    return this.form!;
  }

  // prepare user register form
  private prepareUserRegisterForm() {
    this.form = this.formBuilder.group({
      user_full_name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
        Validators.pattern(/^[A-Za-z0-9]+[\sa-zA-Z0-9]*$/)
      ],
      ],
      user_phone: ['', [
        Validators.required, Validators.pattern(/^\d{10}$/)
      ]],
      user_email: ['', [
        Validators.required,
        Validators.email,
        Validators.maxLength(50),
        Validators.pattern(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/)
      ]],
      user_permission_group: [''],
      user_address_type: [''],
      user_area_and_street: ['', [
        Validators.pattern(/(^[a-zA-Z0-9])+([\sa-zA-Z0-9,.()"&/'\\*!#_=+?:;-])*$/),
        Validators.minLength(3),
        Validators.maxLength(200)
      ]],
      user_city: ['', [
        Validators.minLength(3),
        Validators.maxLength(20),
        Validators.pattern(/^[A-Za-z]+[\sa-zA-Z]*$/)
      ]],
      user_state: [''],
      user_pincode: ['', [Validators.pattern(/^[0-9]{6}$/)]],
      user_password: ['', [
        Validators.required,
        this.patternValidator(/\d/, {hasNumber: true}),
        this.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
        this.patternValidator(/[a-z]/, {hasSmallCase: true}),
        this.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {hasSpecialCharacters: true}),
        Validators.minLength(8),
        Validators.maxLength(26)
      ]],
      user_confirm_password: ['', [Validators.required]]
    }, {validator: UserFormValidatorService.passwordValidator})
  }

  // prepare user edit form
  private prepareUserEditForm(): void {
    this.form?.clearValidators();
    this.form?.updateValueAndValidity();
    this.form?.removeControl("user_password");
    this.form?.removeControl("user_confirm_password");
  }

  // password validator
  private static passwordValidator(control: AbstractControl) {
    const password: string = control.get("user_password")?.value;
    const repeatPassword: string = control.get("user_confirm_password")?.value;
    if (password !== repeatPassword) {
      control.get("user_confirm_password")?.setErrors({NoPassswordMatch: true});
    }
  }
}

export const USER_FORM_VALIDATOR_TOKEN = new InjectionToken<UserFormValidatorService>('UserFormValidatorService', {
  providedIn: 'root',
  factory: () => new UserFormValidatorService(new FormBuilder())
});
