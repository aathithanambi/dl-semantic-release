// Import Angular Core
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Import Components
import {UserViewComponent} from './user_view/user.view.component';
import {UserDetailComponent} from './user_detail/user.detail.component';
import {UserRegisterComponent} from './user_register/user.register.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full'
  },
  {
    path: '',
    data: {
      title: 'Users Home'
    },
    children: [
      {
        path: 'view',
        component: UserViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: UserDetailComponent,
        data: {
          title: 'Detail'
        }
      },
      {
        path: 'register',
        component: UserRegisterComponent,
        data: {
          title: 'Register'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
