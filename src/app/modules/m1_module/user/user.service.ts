// Import Angular Core
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {map} from 'rxjs/operators';
// Import Internal Modules
import {UserType} from "./user.type";
import {environment} from "../../../../environments/environment";
import {AllResponseType} from "../../../core/types/response_type";

const LOCAL_STORAGE_KEY = 'ta_admin_user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  loginUrl = environment.apiUrl + 'm1/dashboard/user/login';
  userUrl = environment.apiUrl + 'm1/dashboard/user/all';
  userByIdUrl = environment.apiUrl + 'm1/dashboard/user/search/?id=';
  userRegisterUrl = environment.apiUrl + 'm1/dashboard/user/register';

  private currentUserSubject: BehaviorSubject<UserType | null>;
  public currentUser: Observable<UserType | null>;

  public get currentUserValue(): UserType | null {
    return this.currentUserSubject.value;
  }

  constructor(private http: HttpClient) {
    const key = localStorage.getItem(LOCAL_STORAGE_KEY);
    this.currentUserSubject = new BehaviorSubject<UserType | null>(
      key ? JSON.parse(key) : null
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // Get all Users
  getUsers(limit?: number, skip?: number): Observable<AllResponseType<UserType>> {
    return this.http.get<AllResponseType<UserType>>(
      this.userUrl + '/?limit=' + (limit || 20) + '&skip=' + (skip || 0) + '&status=all'
    );
  }

  // Get User by ID
  getUser(id: string): Observable<AllResponseType<UserType>> {
    return this.http.get<AllResponseType<UserType>>(this.userByIdUrl + id + '&status=all');
  }

  // login user
  loginUser(user: UserType): Observable<UserType> {
    const body = JSON.stringify(user);

    return this.http.post<UserType>(this.loginUrl, body).pipe(
      map(user => {
        // login successful if there's a jwt token in the response
        if (user.user_token) {
          // store user details and token in local storage to keep user logged in between page refreshes
          localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      })
    );
  }

  // logout user
  logoutUser() {
    // remove user from local storage to log user out
    localStorage.removeItem(LOCAL_STORAGE_KEY);
    this.currentUserSubject.next({});
  }

  // register user
  registerUser(userData: UserType): Observable<UserType> {
    return this.http.post<UserType>(this.userRegisterUrl, userData);
  }

}
