// Import Core
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";
// Import Types
import {UserType} from '../user.type';
// Import Services
import {UserService} from '../user.service';
import {AlertService} from "../../../../core/alert/alert.service";
import {Subscription} from "rxjs";

@Component({
  templateUrl: 'user.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class UserDetailComponent implements OnInit, OnDestroy {
  isLoading: boolean = true;
  userInformation?: UserType;
  isActive?: boolean;
  userSubscription?: Subscription;

  // User Status
  get userStatus(): string {
    return this.isActive ? "Active" : "InActive"
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly userService: UserService,
              private readonly alertService: AlertService
              ) {
  }

  ngOnInit() {
    this.userSubscription = this.userService.getUser(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.userInformation = response?.entityData?.[0];
        this.isLoading = false;
        this.isActive = this.userInformation?.user_status_is_active;
      }, (error) => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  // Switch toggle
  toggleActive(): void {
    this.isActive = !this.isActive
  }

  // edit button handler
  edit(): void {
    alert('edit button clicked');
  }

  // back button handler
  back(): void {
    this.location.back();
  }

  // delete button handler
  delete(): void {
    alert('delete button clicked');
  }

  // reset button handler
  resetPassword(): void {
    alert('reset password button clicked');
  }
}
