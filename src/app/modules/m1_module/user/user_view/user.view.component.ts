// Import External Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from 'rxjs';
// Import Internal Modules
import {UserService} from "../user.service";
import {UserType} from "../user.type";
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'user.view.component.html'
})
export class UserViewComponent implements OnInit, OnDestroy {
  users?: UserType[];
  isLoading: boolean = true;
  userSubscription?: Subscription;
  readonly tableTitle = 'Users';
  readonly columnDefinition = [
    {id: 'user_full_name', name: 'User Name', type: 'string'},
    {id: 'user_email', name: 'User Email', type: 'string'},
    {id: 'user_phone', name: 'User Phone', type: 'string'},
    {id: 'user_created_date', name: 'User Created Date', type: 'date'}
  ];

  constructor(private readonly userService: UserService,
              private readonly alertService: AlertService,
              private readonly router: Router) {
  }

  ngOnInit() {
    this.userSubscription = this.userService.getUsers().subscribe(
      (response) => {
        this.users = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        /** @todo Bug: Error Toaster is not displayed */
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this?.userSubscription?.unsubscribe()
  }
}
