// Angular Core
import {NgModule} from '@angular/core';
// Import Modules
import {UserModule} from './user/user.module';
import {M1RoutingModule} from "./m1.routing.module";

@NgModule({
  imports: [
    UserModule,
    M1RoutingModule
  ]
})
export class M1Module {
}
