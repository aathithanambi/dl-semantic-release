// Angular Core
import {NgModule} from '@angular/core';
// Import Modules
import {VendorModule} from './vendor/vendor.module';
import {M2RoutingModule} from "./m2.routing.module";

@NgModule({
  imports: [
    VendorModule,
    M2RoutingModule
  ]
})
export class M2Module {
}
