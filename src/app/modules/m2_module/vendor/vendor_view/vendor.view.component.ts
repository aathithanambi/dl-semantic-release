// Import External Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from 'rxjs';
// Import Internal Modules
import {VendorService} from "../vendor.service";
import {VendorType} from "../vendor.type";
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'vendor.view.component.html'
})
export class VendorViewComponent implements OnInit, OnDestroy {
  vendors?: VendorType[];
  isLoading: boolean = true;
  vendorSubscription?: Subscription;
  readonly tableTitle = 'Vendors';
  readonly columnDefinition = [
    {id: 'vendor_contact_full_name', name: 'Vendor Contact Name', type: 'string'},
    {id: 'vendor_contact_email', name: 'Vendor Contact Email', type: 'string'},
    {id: 'vendor_contact_phone', name: 'Vendor Contact Phone', type: 'string'},
    {id: 'vendor_created_date', name: 'Vendor Created Date', type: 'date'}
  ];

  constructor(private readonly vendorService: VendorService,
              private readonly alertService: AlertService,
              private readonly router: Router) {
  }

  ngOnInit() {
    this.vendorSubscription = this.vendorService.getVendors().subscribe(
      (response) => {
        this.vendors = response.entityData;
        this.isLoading = false;
      },
      (error) => {
        this.alertService.error(error.message);
        this.router.navigateByUrl('/');
      }
    )
  }

  ngOnDestroy() {
    this.vendorSubscription?.unsubscribe();
  }
}
