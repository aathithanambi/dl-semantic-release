// Import Core Modules
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Import Internal Modules
import {VendorViewComponent} from './vendor_view/vendor.view.component';
import {VendorDetailComponent} from './vendor_detail/vendor.detail.component';
import {VendorRegisterComponent} from './vendor_register/vendor.register.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full'
  },
  {
    path: '',
    data: {
      title: 'Vendors Home'
    },
    children: [
      {
        path: 'view',
        component: VendorViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'detail/:id',
        component: VendorDetailComponent,
        data: {
          title: 'Detail'
        }
      },
      {
        path: 'register',
        component: VendorRegisterComponent,
        data: {
          title: 'Register'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule {
}
