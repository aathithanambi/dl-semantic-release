// Import Internal Modules
import {PermissionGroupType} from "../../m0_module/permission_group/permission_group.type";

export class VendorType {
  _id?: string;
  vendor_no?: string;
  vendor_company_name?: string;
  vendor_contact_full_name?: string;
  vendor_contact_email?: string;
  vendor_password?: string;
  vendor_contact_phone?: string;
  vendor_permission_group?: Partial<PermissionGroupType>;
  vendor_status_is_active?: boolean;
  vendor_last_logged_in?: Date;
  vendor_created_date?: Date;
  vendor_modified_date?: Date;
  vendor_token?: string;
  entityData?: Array<VendorType>;
  token?: string;

  [key: string]: string | number | boolean | Date | Partial<PermissionGroupType> | Array<VendorType> | undefined;
}
