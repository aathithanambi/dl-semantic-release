// Import Core Modules
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {NgSelectModule} from '@ng-select/ng-select';
// Import Internal Modules
import {VendorRoutingModule} from './vendor.routing.module';
import {VendorViewComponent} from './vendor_view/vendor.view.component'
import {VendorDetailComponent} from './vendor_detail/vendor.detail.component'
import {SharedModule} from '../../shared/shared.module';
import {VendorService} from './vendor.service';
import {VendorRegisterComponent} from './vendor_register/vendor.register.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressBarModule,
    NgSelectModule,
    ReactiveFormsModule,
    SharedModule,
    VendorRoutingModule,
  ],
  declarations: [
    VendorDetailComponent,
    VendorRegisterComponent,
    VendorViewComponent,
  ],
  providers: [VendorService]
})
export class VendorModule {
}
