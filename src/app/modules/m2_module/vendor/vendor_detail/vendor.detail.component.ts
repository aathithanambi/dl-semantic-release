// Import Core Modules
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Location} from '@angular/common';
// Import Internal Modules
import {VendorType} from '../vendor.type';
import {VendorService} from '../vendor.service';
import {AlertService} from "../../../../core/alert/alert.service";

@Component({
  templateUrl: 'vendor.detail.component.html',
  styleUrls: ['../../../../../scss/_details.scss'],
})
export class VendorDetailComponent implements OnInit, OnDestroy {
  isLoading: boolean = true;
  vendorInformation?: VendorType;
  vendorSubscription?: Subscription;
  isActive: boolean = false;

  // vendor Status
  get vendorStatus(): string {
    return this.isActive ? 'Active' : 'InActive';
  }

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly location: Location,
              private readonly vendorService: VendorService,
              private readonly alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.vendorSubscription = this.vendorService.getVendor(this.route.snapshot.params.id).subscribe(
      (response) => {
        this.vendorInformation = response.entityData[0];
        this.isLoading = false;
        this.isActive = this.vendorInformation?.vendor_status_is_active!;
      }, (error => {
        this.alertService.error(error.error);
        this.router.navigateByUrl('/');
      })
    )
  }

  ngOnDestroy() {
    this.vendorSubscription?.unsubscribe();
  }

  // switch toggle
  toggleActive(): void {
    this.isActive = !this.isActive;
  }

  // edit button handler
  edit(): void {
    alert('edit button clicked');
  }

  // back button handler
  back(): void {
    this.location.back();
  }

  // delete button handler
  delete(): void {
    alert('delete button clicked');
  }

}
