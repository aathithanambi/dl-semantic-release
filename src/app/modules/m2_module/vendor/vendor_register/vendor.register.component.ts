// Import Core Modules
import {Component, Inject} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {forkJoin} from 'rxjs';
// Import Internal Modules
import {VendorService} from '../vendor.service';
import {AlertService} from '../../../../core/alert/alert.service';
import {VENDOR_FORM_VALIDATOR_TOKEN} from '../vendor.validators';
import {AbstractValidator} from '../../../../core/validator/validator';
import {AbstractRegister} from '../../../../core/modules/register';
import {VendorType} from '../vendor.type';
import {PermissionGroupService} from '../../../m0_module/permission_group/permission_group.service';
import {MetadataService} from '../../../m0_module/metadata/metadata.service';

@Component({
  templateUrl: 'vendor.register.component.html',
  styleUrls: ['../../../../../scss/_registers.scss'],
})
export class VendorRegisterComponent extends AbstractRegister<VendorType> {
  registerForm?: FormGroup;
  private vendorRegisterFields = [
    'vendor_contact_full_name',
    'vendor_contact_email',
    'vendor_password',
    'vendor_contact_phone',
    'vendor_company_name'
  ];
  addressTypes = ['Address Type 1', 'Address Type 2'];
  permissionGroups: string[] = [];
  sequence: string = 'Error';

  constructor(private router: Router,
              private location: Location,
              private vendorService: VendorService,
              private alertService: AlertService,
              @Inject(VENDOR_FORM_VALIDATOR_TOKEN) private vendorRegisterValidator: AbstractValidator,
              permissionGroupService: PermissionGroupService,
              metadata: MetadataService) {
    super();
    this.showProgressBar(true);
    // Load Register Form data
    forkJoin({
      permissionGroups: permissionGroupService.getPermissionGroups(),
      metadata: metadata.getMetadata('vendors')
    }).pipe(takeUntil(this.destroy$)).subscribe((response) => {
      this.permissionGroups = response.permissionGroups.entityData.map((permissionGroup) => permissionGroup.permission_group_name!);
      this.sequence = response.metadata.sequence;
    }, (error) => {
      this.alertService.error(error.error);
      this.router.navigateByUrl('m2/vendor/view');
    }).add(() => this.showProgressBar(false));
  }

  // get the registerForm
  ngOnInit() {
    this.registerForm = this.vendorRegisterValidator.buildForm();
  }

  // form submit handler
  submit(): void {
    const vendorFormData = this.registerForm?.value;
    let vendor = this.getFormData(vendorFormData, this.vendorRegisterFields);
    this.showProgressBar(true, 'Registering New Vendor');
    this.vendorService.registerVendor(vendor)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (vendor: VendorType) => {
          this.alertService.success(`Thank you ${vendor.vendor_contact_full_name}`);
          this.registerForm?.reset();
          this.router.navigateByUrl('m2/vendor/view');
        },
        (error) => {
          this.registerForm?.get('vendor_password')?.setValue('');
          this.registerForm?.get('vendor_confirm_password')?.setValue('');
          this.alertService.error(error.error);
        },
      ).add(() => this.showProgressBar(false));
  }

  // back button handler
  back(): void {
    this.location.back();
  }

}
