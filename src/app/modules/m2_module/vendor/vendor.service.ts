// Import Core Modules
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
// Import Internal Modules
import {VendorType} from "./vendor.type";
import {environment} from "../../../../environments/environment";
import {AllResponseType} from '../../../core/types/response_type';

@Injectable({
  providedIn: 'root',
})
export class VendorService {
  vendorUrl = environment.apiUrl + 'm2/dashboard/vendor/all';
  vendorByIdUrl = environment.apiUrl + 'm2/dashboard/vendor/search/?id=';
  vendorRegisterUrl = environment.apiUrl + 'm2/dashboard/vendor/register';
  vendorUpdateUrl = environment.apiUrl + 'm2/dashboard/vendor/update/';

  constructor(private http: HttpClient) {
  }

  // get all vendors
  getVendors(): Observable<AllResponseType<VendorType>> {
    return this.http.get<AllResponseType<VendorType>>(this.vendorUrl);
  }

  // get vendor by id
  getVendor(id: string): Observable<AllResponseType<VendorType>> {
    return this.http.get<AllResponseType<VendorType>>(this.vendorByIdUrl + id);
  }

  // register vendor
  registerVendor(data: VendorType) {
    return this.http.post<VendorType>(this.vendorRegisterUrl, data);
  }

  // update vendor

  updateVendor(id: string, data: VendorType): Observable<VendorType> {
    return this.http.put<VendorType>(this.vendorUpdateUrl + id, data);
  }

}
