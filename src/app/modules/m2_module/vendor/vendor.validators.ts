// Import Core Modules
import {InjectionToken} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
// Import Internal Modules
import {AbstractValidator} from '../../../core/validator/validator';

class VendorFormValidatorService extends AbstractValidator {
  form?: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  buildForm(formType?: 'edit') {
    this.prepareVendorRegisterForm();
    if (formType === 'edit') {
      this.prepareVendorEditForm();
    }
    return this.form!;
  }

  // prepare vendor register form
  private prepareVendorRegisterForm() {
    this.form = this.formBuilder.group({
      vendor_contact_full_name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
        Validators.pattern(/^[A-Za-z0-9]+[\sa-zA-Z0-9]*$/)
      ],
      ],
      vendor_contact_phone: ['', [
        Validators.required, Validators.pattern(/^\d{10}$/)
      ]],
      vendor_contact_email: ['', [
        Validators.required,
        Validators.email,
        Validators.pattern(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/),
        Validators.maxLength(50)
      ]],
      vendor_permission_group: [''],
      vendor_company_name: ['', [
        Validators.required,
        Validators.pattern(/(^[a-zA-Z0-9])+([\sa-zA-Z0-9,.()"&/'\\*!#_=+?:;-])*$/),
        Validators.minLength(3),
        Validators.maxLength(20)
      ]],
      vendor_address_type: [''],
      vendor_area_and_street: ['', [
        Validators.pattern(/(^[a-zA-Z0-9])+([\sa-zA-Z0-9,.()"&/'\\*!#_=+?:;-])*$/),
        Validators.minLength(3),
        Validators.maxLength(200)
      ]],
      vendor_city: ['', [
        Validators.minLength(3),
        Validators.maxLength(20),
        Validators.pattern(/^[A-Za-z]+[\sa-zA-Z]*$/)
      ]],
      vendor_state: [''],
      vendor_pincode: ['', [Validators.pattern(/^[0-9]{6}$/)]],
      vendor_password: ['', [
        Validators.required,
        this.patternValidator(/\d/, {hasNumber: true}),
        this.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
        this.patternValidator(/[a-z]/, {hasSmallCase: true}),
        this.patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {hasSpecialCharacters: true}),
        Validators.minLength(8),
        Validators.maxLength(26)
      ]],
      vendor_confirm_password: ['', [Validators.required]]
    }, {validator: VendorFormValidatorService.passwordValidator});
  }

  // prepare vendor edit form
  private prepareVendorEditForm(): void {
    this.form?.clearValidators();
    this.form?.updateValueAndValidity();
    this.form?.removeControl('vendor_password');
    this.form?.removeControl('vendor_confirm_password');
  }

  // password validator
  private static passwordValidator(control: AbstractControl) {
    const password: string = control.get('vendor_password')?.value;
    const repeatPassword: string = control.get('vendor_confirm_password')?.value;
    if (password !== repeatPassword) {
      control.get('vendor_confirm_password')?.setErrors({NoPassswordMatch: true});
    }
  }
}

export const VENDOR_FORM_VALIDATOR_TOKEN = new InjectionToken<VendorFormValidatorService>('VendorFormValidatorService', {
  providedIn: 'root',
  factory: () => new VendorFormValidatorService(new FormBuilder())
});
