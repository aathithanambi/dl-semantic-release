interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Modules'
  },
  {
    name: 'Users',
    url: '/m1/user',
    icon: 'icon-user'
  },
  {
    name: 'Vendors',
    url: '/m2/vendor',
    icon: 'icon-user'
  },
  {
    name: 'Catalog',
    url: '/m3/catalog',
    icon: 'icon-list',
    children: [
      {
        name: 'Products',
        url: '/m3/product',
        icon: 'icon-star'
      },
      {
        name: 'Product Categories',
        url: '/m3/product_category',
        icon: 'icon-star'
      },
      {
        name: 'Product Reviews',
        url: '/m3/product_review',
        icon: 'icon-star'
      },
      {
        name: 'Product Brands',
        url: '/m3/product_brand',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Promotions',
    url: '/m6/promotion',
    icon: 'icon-list',
    children: [
      {
        name: 'Promotion',
        url: '/m6/promotion',
        icon: 'icon-star'
      },
      {
        name: 'Promotion Category',
        url: '/m6/promotion_category',
        icon: 'icon-star'
      }
    ]
  }
];
