/* SystemJS module definition */
declare var module: NodeModule;
declare module '@coreui/coreui/dist/js/coreui-utilities';
declare module '@coreui/coreui-plugin-chartjs-custom-tooltips';
interface NodeModule {
  id: string;
}
