// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'http://api.rel.shop.techardors.com/api/',
  token: {
  admin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoidXNlciIsImlkIjoiNWUzN2VhOWQyZDYzYTczYzM1Zjg4ZmQzIiwicGVybWlzc2lvbkdyb3VwIjp7Il9pZCI6IjVlMzdlYWIwYTEwNjhhM2JmODZkODYzOSIsInBlcm1pc3Npb25fZ3JvdXBfbmFtZSI6ImFkbWluIn0sImlhdCI6MTU4MTA3NTU1NH0._Gdq655p4hmhnQ4fuVDS14fd7VmsB34edFpM-VK-vYg',
  app: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoidXNlciIsImlkIjoiNWUzN2VhOWUyZDYzYTczYzM1Zjg4ZmQ0IiwicGVybWlzc2lvbkdyb3VwIjp7Il9pZCI6IjVlMzdlYWIwYTEwNjhhM2JmODZkODYzOCIsInBlcm1pc3Npb25fZ3JvdXBfbmFtZSI6ImFwcCJ9LCJpYXQiOjE1ODEwNzU1Mjl9.wuTKEvmUacgnqOyDGgqK_kV1rMt8Gvy5Ix2eQMFdLZQ',
  }
};
