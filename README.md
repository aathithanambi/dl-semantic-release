## E-Commerce Admin Dashboard

## About Project
The Project facilitates the Admin to maintain the E-Commerce Website via Dashboard.
 
Following are the few of the list of actions Admin can perform:
* User Management
* Vendor Management
* Catalog(Product) Management
* Inventory Management
* Ads and Promotion Management

## Tech Stack
Following are the tools and technologies used in this project:
* **CoreUI** - Angular Dashboard Theme.
* **Angular 8** - To maintain the frontend of the Application.
* **NodeJS v12.0** - To run the application.
* **NPM** - Package Manager.
* **Git** - Version Control Management.
* **Confluence** - Project Documentation
* **GitLab** - Issue Tracking.
* **VS Code** - Recommended IDE for Development.
* **GitLab CI/CD** - Auto Deployment.
* **AWS** - Cloud server to host the Application.

## CoreUI
* Version Installed - 2.5.2
* GitHub Link - https://github.com/coreui/coreui-free-angular-admin-template

#### Prerequisites
Before you begin, make sure your development environment includes `Node.js®` and an `npm` package manager.

###### Node.js
Angular 8 requires `Node.js` version 12.x   
- To check your version, run `node -v` in a terminal/console window.

###### Angular CLI
Install the Angular CLI globally using a terminal/console window.
```bash
npm install -g @angular/cli
```

## Usage

``` bash
# serve with hot reload at localhost:4200.
$ ng serve

# build for production with minification.
$ ng build
```

## Folder Structure

```
ecomm_admin_dashboard/
├── e2e/
├── src/
│   ├── app/
│   ├── assets/
│   ├── environments/
│   ├── scss/
│   ├── index.html
│   └── ...
├── .angular-cli.json
├── ...
├── package.json
└── ...
```
## Documentation

The documentation for the CoreUI Free Angularp Admin Template is hosted at our website [CoreUI](https://coreui.io/angular/)
