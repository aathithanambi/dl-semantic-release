#!/bin/sh

if [ "$(sudo docker ps -q -f name=ecomm-admin-latest)" ] || [ "$(sudo docker ps -aq -f status=exited -f name=ecomm-admin-latest)" ]; then
  echo "Running Docker Cleanup"
    sudo docker stop ecomm-admin-latest
    sudo docker rm ecomm-admin-latest
    sudo docker system prune -f
    sudo docker image prune -f
  echo "Docker Cleanup Succeeded"
fi
