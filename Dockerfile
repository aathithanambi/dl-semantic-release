# ***** @techardors - This configuration is for development and not production ready  *****
FROM node:12.13
 
# Create app directory
WORKDIR /usr/src/app

# Copy the files
COPY . .

# Run the npm commands
RUN npm install

# Start the development/staging server
EXPOSE 4200 
CMD [ "npm", "start" ]